#include<stdio.h>
 #include<math.h>
 struct point
 {
	float x;
    float y;
 };
 typedef struct point Point;
 Point input()
 {
	Point p;
    printf("enter two points\n");
    scanf("%f%f",&p.x,&p.y);
    return p;
 }
 float distance(Point p1,Point p2)
 {
 	float d;
 	d=sqrt(pow(p2.x-p1.x,2)+pow(p2.y-p1.y,2));
 	return d;
 }
 void output(Point p1,Point p2,float distance)
 {
	 printf("the distance between %f\t%f and %f\n%f is %f\n",p1.x,p2.x,p1.y,p2.y,distance);
 }
 int main()
 {
 	Point x,y;
	float z;
	x=input();
 	y=input();
 	z=distance(x,y);
 	output(x,y,z);
 	return 0;
 }
 