#include<stdio.h>
void swap(int *a,int *b)
{
    int temp;
    temp=*a;
    *a=*b;
    *b=temp;
}
int main()
{
    int n1,n2;
    printf("Enter two numbers:\n");
    scanf("%d %d",&n1,&n2);
    printf("The value of n1 and n2 before swapping is : %d %d\n",n1,n2);
    swap(&n1,&n2);
    printf("The value of n1 and n2 after swapping is :  %d %d\n",n1,n2);
    return 0;
}
