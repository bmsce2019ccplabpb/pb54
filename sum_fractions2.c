#include<stdio.h>
int input()
{
    int num;
    printf("Enter the length of fraction series ");
    scanf("%d",&num);
    return num;
}
float compute(int num)
{
    int i;
    float temp,sum=0.0;
    for(i=1;i<=num;i++)
    {
        temp=1.0/i;
        sum=sum+temp;
    }
        return sum;
}
void output(int num,float sum)
{
    printf("the sum of %d fractions is %f",num,sum);
}
int main()
{
    int x;
    float y;
    x=input();
    y=compute(x);
    output(x,y);
    return 0;
}