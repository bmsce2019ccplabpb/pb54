#include<stdio.h>
int sum_digits(int num);
int main()
{
	int num,sum=0;
	printf("Enter a number\n");
	scanf("%d",&num);
	sum=sum_digits(num);
	printf("sum of %d is %d\n",num,sum);
	return 0;
}
int sum_digits(int num)
{
	int sum=0,temp;
	while(num!=0)
		{
		temp=num%10;
		sum=sum+temp;
		num=num/10;
		}
		return sum;
}