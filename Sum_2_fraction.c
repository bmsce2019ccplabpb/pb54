#include<stdio.h>
struct fraction
{
    float x;
    float y;
};
typedef struct fraction Fraction;
Fraction input()
{
    Fraction f;
    printf("Enter the numerator\n");
    scanf("%f",&f.x);
    printf("Enter the denominator\n");
    scanf("%f",&f.y);
    return f;
}
float compute(Fraction f1,Fraction f2)
{
    float sum=0.0;
    sum=(f1.x/f1.y)+(f2.x/f2.y);
    return sum;
}
void output(Fraction f1,Fraction f2,float sum)
{
    printf("the sum of f1 and f2 is %f\n",sum);
}
int main()
{
    Fraction x,y;
    float z;
    x=input();
    y=input();
    z=compute(x,y);
    output(x,y,z);
    return 0;
}